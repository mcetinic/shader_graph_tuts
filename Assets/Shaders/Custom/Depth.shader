﻿Shader "Shaders/Custom/Depth"
{
	HLSLINCLUDE

	#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

	TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);	

	bool _LinearSpace;

	sampler2D _CameraDepthTexture;

	float4 Frag(VaryingsDefault i) : SV_Target
	{		
		float depth = tex2D(_CameraDepthTexture, i.texcoord).r;		

		if (_LinearSpace)
		{
			depth = Linear01Depth(depth);
			//depth = depth * _ProjectionParams.z;
		}

		return depth;
	}

	ENDHLSL

	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			HLSLPROGRAM

			#pragma vertex VertDefault
			#pragma fragment Frag

			ENDHLSL
		}
	}
}