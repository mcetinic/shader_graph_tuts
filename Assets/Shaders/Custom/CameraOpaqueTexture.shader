﻿Shader "Custom/CameraOpaqueTexture"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_MainColor("MainColor", Color) = (1,1,1,1)		
		_Distort("Distort", Range(0, 100)) = 1.0
		[HDR] _Color("Color", Color) = (0,0,0,0)
		[HDR] _GlowColor("Glow Color", Color) = (1, 1, 1, 1)
		_FadeLength("Intersection Fade Length", Range(0, 2)) = 0.15
	}

	SubShader
	{
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite On

		//Tags { "RenderType" = "Opaque" }
		Tags{ "RenderType" = "Transparent" "IgnoreProjector" = "True" "Queue" = "Transparent"}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag            

			#include "UnityCG.cginc"

			struct appdata
			{
				fixed4 vertex : POSITION;
				fixed4 uv : TEXCOORD0;	
				fixed3 normal : NORMAL;
			};

			struct v2f
			{				
				half2 uv : TEXCOORD0;	
				float3 objectPosition : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float3 worldNormal : NORMAL;
			};

			sampler2D _MainTex;
			sampler2D _CameraOpaqueTexture;
			sampler2D _CameraDepthTexture;

			fixed4 _MainTex_ST;
			fixed4 _MainColor;
			fixed4 _CameraOpaqueTexture_ST;
			fixed4 _CameraOpaqueTexture_TexelSize;

			fixed _Distort;
			fixed4 _Color;
			fixed3 _GlowColor;
			fixed _FadeLength;

			v2f vert(appdata v, out float4 vertex : SV_POSITION)
			{
				v2f o;

				vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);				
				o.objectPosition = v.vertex;
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.viewDir = ObjSpaceViewDir(v.vertex);

				return o;
			}
			
			fixed4 frag(v2f i, UNITY_VPOS_TYPE vpos : VPOS) : SV_Target
			{
				float2 screenuv = vpos.xy / _ScreenParams.xy;
				float screenDepth = Linear01Depth(tex2D(_CameraDepthTexture, screenuv));
				float diff = screenDepth - Linear01Depth(vpos.z);
				float intersect = 0;

				if (diff > 0)
					intersect = 1 - smoothstep(0, _ProjectionParams.w * _FadeLength, diff);

				fixed4 glowColor = fixed4(lerp(_Color.rgb, _GlowColor, pow(intersect, 4)), 1);

				fixed4 col = _Color * _Color.a + glowColor;
				col.a = _Color.a;				

				/*screenuv += (mainText.rg * 2 - 1) * _Distort * _CameraOpaqueTexture_TexelSize.xy;
				fixed3 distortColor = tex2D(_CameraOpaqueTexture, screenuv);
				distortColor *= _MainColor * _MainColor.a + 1;*/

				return col;// col;
			}
			ENDCG
		}
	}
}
