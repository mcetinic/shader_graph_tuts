﻿Shader "Custom/Intersection"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off

		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}       

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
				float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;      
				float3 viewDir : TEXCOORD1;                
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.viewDir = ObjSpaceViewDir(v.vertex);

                return o;
            }

			sampler2D _CameraDepthTexture;

			void Unity_Remap_float4(float4 In, float2 InMinMax, float2 OutMinMax, out float4 Out)
			{
				Out = OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
			}

            fixed4 frag (v2f i) : SV_Target
            {
				//fixed4 col = tex2D(_MainTex, i.uv);

				fixed3 fragToCameraDiff = i.vertex.xyz - _WorldSpaceCameraPos;
				
				float dotDirAndDiff = dot(i.viewDir, fragToCameraDiff);

				_ProjectionParams.z;

                return fixed4(fragToCameraDiff, 1.0);
            }
            ENDCG
        }
    }
}
