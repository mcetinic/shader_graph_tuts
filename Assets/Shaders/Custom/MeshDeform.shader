﻿Shader "Custom/MeshDeform"
{
	Properties
	{
		_MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Color", Color) = (0,0,0,0)		

		_HitColor("Hit Color", Color) = (1, 1, 1, 1)
		_HitEffectBorder("Hit Effect Border", Range(0.01, 0.25)) = 0.05
	}

	SubShader
	{		
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite On

		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}

		Pass
		{
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;			
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			int _HitsCount = 0;
			float _HitsRadius[10];
			float3 _HitsObjectPosition[10];
			float _HitsIntensity[10];

			float _HitEffectBorder;

			float DrawRing(float intensity, float radius, float dist)
			{
				float currentRadius = lerp(0, radius, 1.0 - intensity);//expand radius over time 
				return intensity * (1 - smoothstep(currentRadius, currentRadius + _HitEffectBorder, dist) - (1 - smoothstep(currentRadius - _HitEffectBorder, currentRadius, dist)));
			}
			
			float CalculateHitsDeformFactor(float3 objectPosition)
			{
				float factor = 0.0;
				for (int i = 0; i < _HitsCount; i++)
				{
					float distanceToHit = distance(objectPosition, _HitsObjectPosition[i]);
					factor += DrawRing(_HitsIntensity[i], _HitsRadius[i], distanceToHit);
				}
				
				factor = saturate(factor);

				return factor;
			}

			v2f vert(appdata v, out float4 vertex : SV_POSITION)
			{
				float3 objectPosition = v.vertex;

				objectPosition *= 1.0 - CalculateHitsDeformFactor(objectPosition);

				v2f o;
				vertex = UnityObjectToClipPos(objectPosition);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);								

				return o;
			}
			
			fixed4 _Color;

			fixed4 frag(v2f i, UNITY_VPOS_TYPE vpos : VPOS) : SV_Target
			{				
				return _Color; 
			}
			ENDCG
		}
	}
}