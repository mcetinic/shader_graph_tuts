﻿shader "Custom/FlowMap_Displacement"
{
	Properties
	{
		_Color("Tint", Color) = (1,1,1,1)
		_MainTex("Sprite Texture", 2D) = "white" {}		
		_FlowMap("FlowMap", 2D) = "white" {}
		_FlowSpeed("Flow Speed", float) = 1.0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				half2 texcoord  : TEXCOORD0;
			};

			fixed4 _Color;

			v2f vert(appdata_t IN)
			{
				v2f OUT;

				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;				

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _FlowMap;
			float _FlowSpeed;

			float Unity_Remap_float4(float In, float2 InMinMax, float2 OutMinMax)
			{
				return OutMinMax.x + (In - InMinMax.x) * (OutMinMax.y - OutMinMax.x) / (InMinMax.y - InMinMax.x);
			}

			fixed4 frag(v2f IN) : SV_Target
			{				
				float phase1 = frac(_Time[0] * _FlowSpeed);
				float phase2 = frac(phase1 + 0.5);

				float2 uv = IN.texcoord;

				float2 distort = tex2D(_FlowMap, uv).rg;

				float2 phase1UV = lerp(distort, uv, phase1);
				float2 phase2UV = lerp(distort, uv, phase2);

				float4 phase1Tex = tex2D(_MainTex, phase1UV);
				float4 phase2Tex = tex2D(_MainTex, phase2UV);				

				float remapedPhase1 = abs( Unity_Remap_float4( phase1, float2(0,1), float2(-1, 1) ) );

				float3 emission = lerp(phase1Tex.rgb, phase2Tex.rgb, remapedPhase1);
				
				float opacity = lerp(phase1Tex.a, phase2Tex.a, remapedPhase1);

				emission *= opacity * _Color;

				return float4(emission, opacity);
			}

			ENDCG
		}
	}
}