﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(DepthRenderer), PostProcessEvent.AfterStack, "Custom/Depth Visualizer")]
public sealed class Depth : PostProcessEffectSettings
{
    [Range(0f, 1f), Tooltip("Linear Depth")]
    public BoolParameter linearSpace = new BoolParameter { value = false };
}