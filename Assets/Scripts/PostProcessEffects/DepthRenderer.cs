﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public sealed class DepthRenderer : PostProcessEffectRenderer<Depth>
{
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Shaders/Custom/Depth"));

        sheet.properties.SetInt("_LinearSpace", settings.linearSpace ? 1 : 0);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}