﻿using UnityEngine.Rendering.PostProcessing;
using UnityEditor.Rendering.PostProcessing;

[PostProcessEditor(typeof(Depth))]
public sealed class DepthEditor : PostProcessEffectEditor<Depth>
{
    SerializedParameterOverride m_LinearSpace;

    public override void OnEnable()
    {
        m_LinearSpace = FindParameterOverride(x => x.linearSpace);
    }

    public override void OnInspectorGUI()
    {
        PropertyField(m_LinearSpace);
    }
}
